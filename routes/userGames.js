// user game
const express = require('express');
const router = express();
const {UserGame} = require('../models')
const {UserGameBiodata} = require('../models')
const {UserGameHistory} = require('../models')

router.use(express.json())

router.get('/usergame', async (req, res) => {
    const userGames = await UserGame.findAll()
    res.json(userGames)
})

router.get('/usergame/:id', (req, res) => {
    UserGame.findOne({
        where: {
            id: req.params.id
        }
    }).then((userGame) => {
        res.json(userGame)
    })
})  

router.post('/usergame', async(req, res) => {
    const userGames = await UserGame.create({
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    })
    
    res.status(201).json(userGames)
})

router.put('/usergame/:id', async(req, res) => {
    const id = req.params.id

    const userGames = await UserGame.update({
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    }, {
        where: {
            id: id
        }
    })
    res.json({
        message: 'Updated user games'
    })
})

router.delete('/usergame/:id', async (req, res) => {
    const id =req.params.id

    const userGames = await UserGame.destroy({
        where: {
            id:id
        }
    })
    res.json({
        message: 'Deleted user games'
    })
})

// end user games

// user game biodata
router.get('/usergamebiodata', async (req, res) => {
    const userGameBiodata = await UserGameBiodata.findAll()
    res.json(userGameBiodata)
})

router.get('/usergamebiodata/:id', (req, res) => {
    UserGameBiodata.findOne({
        where: {
            id: req.params.id
        }
    }).then((userGameBiodata) => {
        res.json(userGameBiodata)
    })
})

router.post('/usergamebiodata', async(req, res) => {
    const userGameBiodata = await UserGameBiodata.create({
        full_name: req.body.score,
        birth_date: req.body.birth_date,
        gender: req.body.gender,
        user_game_id: req.body.user_game_id
    })
    
    res.status(201).json(userGameBiodata)
})

router.put('/usergamebiodata/:id', async(req, res) => {
    const id = req.params.id

    const userGameBiodata = await UserGameBiodata.update({
        full_name: req.body.score,
        birth_date: req.body.birth_date,
        gender: req.body.gender,
        user_game_id: req.body.user_game_id
    }, {
        where: {
            id: id
        }
    })
    res.json({
        message: 'Updated article'
    })
})

router.delete('/usergamebiodata/:id', async (req, res) => {
    const id =req.params.id

    const userGameBiodata = await UserGameBiodata.destroy({
        where: {
            id:id
        }
    })
    res.json({
        message: 'Deleted article'
    })
})

// end user game biodata

//start user game histories
router.get('/usergamehistory', async (req, res) => {
    const UserGameHistories = await UserGameHistory.findAll()
    res.json(UserGameHistories)
})

router.get('/usergamehistory/:id', (req, res) => {
    UserGameHistory.findOne({
        where: {
            id: req.params.id
        }
    }).then((UserGameHistories) => {
        res.json(UserGameHistories)
    })
})

router.post('/usergamehistory', async(req, res) => {
    const UserGameHistories = await UserGameHistory.create({
        score: req.body.score,
        user_game_id: req.body.user_game_id
    })
    
    res.status(201).json(UserGameHistories)
})

router.put('/usergamehistory/:id', async(req, res) => {
    const id = req.params.id

    const UserGameHistories = await UserGameHistory.update({
        score: req.body.score,
        user_game_id: req.body.user_game_id
    }, {
        where: {
            id: id
        }
    })
    res.json({
        message: 'Updated article'
    })
})

router.delete('/usergamehistory/:id', async (req, res) => {
    const id =req.params.id

    const UserGameHistories = await UserGameHistory.destroy({
        where: {
            id:id
        }
    })
    res.json({
        message: 'Deleted article'
    })
})
// end user game histories

// website render

router.get('/alldashboard', (req, res) => {
    res.render('alldashboard')
})

// user games
router.get('/usergames', (req, res) => {
    UserGame.findAll()
     .then(userGames => {
     res.render('dashboard/usergames-dashboard', {
     userGames
     })
 })
})

router.get('/usergames/create', (req, res) => {
    res.render('dashboard/usergames-create')
})

router.post('/usergames/create', async(req, res) => {
    const userGames = await UserGame.create({
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
    })
    .then(userGames => {
        res.status(201).redirect('/admin/usergames')
    })
})

router.get('/usergames/update/:id', async (req, res) => {
    const id = req.params.id
    const userGame = await UserGame.findByPk(id)
    res.render('dashboard/usergames-update', 
    {i: userGame})
})

router.post('/usergames/update/:id', async(req, res) => {
    const id = req.params.id

    const userGames = await UserGame.update({
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    }, {
        where: {
            id: id
        }
    })
    .then(userGames => {
        res.redirect('/admin/usergames')
    })
})


router.get('/usergames/delete/:id', async (req, res) => {
    const id = req.params.id
    const userGame = await UserGame.findByPk(id)
    res.render('dashboard/usergames-delete', 
    {i: userGame})
})

router.post('/usergames/delete/:id', async (req, res) => {
    const id = req.params.id;

    try {
        await UserGame.destroy({
            where: {
                id: id
            }
        });
        res.redirect('/admin/usergames');
    } catch (err) {
        res.status(500).send(err);
    }
});

// user game biodata
router.get('/userbiodata', (req, res) => {
    UserGameBiodata.findAll()
     .then(userGameBiodata => {
     res.render('dashboard/biodata-dashboard', {
     userGameBiodata
     })
 })
})

router.get('/userbiodata/create', (req, res) => {
    res.render('dashboard/biodata-create')
})

router.post('/userbiodata/create', async(req, res) => {
    const userGamesBiodata = await UserGameBiodata.create({
        full_name: req.body.full_name,
        birth_date: req.body.birth_date,
        gender: req.body.gender,
        user_game_id: req.body.user_game_id
    })
    .then(userGamesBiodata => {
        res.status(201).redirect('/admin/userbiodata')
    })
})
 
router.get('/userbiodata/update/:id', async (req, res) => {
    const id = req.params.id
    const userGameBiodata = await UserGameBiodata.findByPk(id)
    res.render('dashboard/biodata-update', 
    {i: userGameBiodata})
})

router.post('/userbiodata/update/:id', async(req, res) => {
    const id = req.params.id

    const userGameBiodata = await UserGameBiodata.update({
        full_name: req.body.full_name,
        birth_date: req.body.birth_date,
        gender: req.body.gender,
        user_game_id: req.body.user_game_id
    }, {
        where: {
            id: id
        }
    })
    .then(userGameBiodata => {
        res.redirect('/admin/userbiodata')
    })
})

router.get('/userbiodata/delete/:id', async (req, res) => {
    const id = req.params.id
    const userGameBiodata = await UserGameBiodata.findByPk(id)
    res.render('dashboard/biodata-delete', 
    {i: userGameBiodata})
})

router.post('/userbiodata/delete/:id', async (req, res) => {
    const id = req.params.id;

    try {
        await UserGameBiodata.destroy({
            where: {
                id: id
            }
        });
        res.redirect('/admin/userbiodata');
    } catch (err) {
        res.status(500).send(err);
    }
});

// user game history
router.get('/userhistories', (req, res) => {
    UserGameHistory.findAll()
     .then(userGameHistories => {
     res.render('dashboard/history-dashboard', {
     userGameHistories
     })
 })
})

router.get('/userhistories/create', (req, res) => {
    res.render('dashboard/history-create')
})

router.post('/userhistories/create', async(req, res) => {
    const userGamesHistories = await UserGameHistory.create({
        score: req.body.score,
        user_game_id: req.body.user_game_id
    })
    .then(userGamesHistories => {
        res.status(201).redirect('/admin/userhistories')
    })
})

router.get('/userhistories/update/:id', async (req, res) => {
    const id = req.params.id
    const userGamesHistories = await UserGameHistory.findByPk(id)
    res.render('dashboard/history-update', 
    {i: userGamesHistories})
})

router.post('/userhistories/update/:id', async(req, res) => {
    const id = req.params.id

    const userGamesHistories = await UserGameHistory.update({
        score: req.body.score,
        user_game_id: req.body.user_game_id
    }, {
        where: {
            id: id
        }
    })
    .then(userGamesHistories => {
        res.redirect('/admin/userhistories')
    })
})

router.get('/userhistories/delete/:id', async (req, res) => {
    const id = req.params.id
    const userGameHistories = await UserGameHistory.findByPk(id)
    res.render('dashboard/history-delete', 
    {i: userGameHistories})
})

router.post('/userhistories/delete/:id', async (req, res) => {
    const id = req.params.id;

    try {
        await UserGameHistory.destroy({
            where: {
                id: id
            }
        });
        res.redirect('/admin/userhistories');
    } catch (err) {
        res.status(500).send(err);
    }
});



module.exports = router

