const express = require('express')
const router = express.Router()
const {UserGame} = require('../models')
const {UserGameBiodata} = require('../models')
const { response } = require('./userGames')

router.use((request, response, next) => {
    next()
})
router.get('/login', (request, response) => {
    response.render('login')
})

router.post('/login', (req, res) => {
    const { username, password } = req.body;
    UserGame.findOne({
        where: {
            username: username,
            password: password
        }
    }).then(user => {
        if (!user) {
            res.render('loginfailed.ejs', {status:'Wrong Email or Password'});
        } else {
            // login success, store user information in session or cookie
            res.redirect('/auth/loginAs');
        }
    });
});

router.get('/loginAs', (req,res) => {
    res.render('login-as')
})

router.get('/register', (request, response) => {
    response.render('register')
})

router.post('/register', async(req, res) => {
    const userGames = await UserGame.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    })
    .then(userGames => {
        res.redirect('/auth/register-biodata')
    })
})

router.get('/register-biodata', (request, response) => {
    response.render('register-biodata')
})

router.post('/register-biodata', async(req, res) => {
    const userGamesBiodata = await UserGameBiodata.create({
        full_name: req.body.full_name,
        birth_date: req.body.birth_date,
        gender: req.body.gender,
        id_user_game: req.body.user_game_id
    })
    .then(userGamesBiodata => {
        res.status(201).redirect('/auth/login')
    })
})

module.exports = router
